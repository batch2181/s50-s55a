import CourseCard from '../components/CourseCard'
import coursesData from "../data/CoursesData";

export default function Courses() {

    console.log(coursesData);
    console.log(coursesData[0]);

    const courses = coursesData.map(course =>{
        return(
            <CourseCard key={course.id} course={course}/>
        )
    })

    return(
        <>
        {courses}
        </>
    )
}

