import { useEffect, useState, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Login() {

    const {user, setUser} = useContext(UserContext)
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isActive, setIsActive] = useState(false)


    function loginUser(e) {
        e.preventDefault()

        localStorage.setItem('email', email);
        setUser({email: localStorage.getItem('email')});

        setEmail("");
        setPassword("");

        alert(`${email} has been verified! Welcome back!`);
    }

    useEffect(() => {
        if((email !== "" && password !== "") && (password.length >= 6)) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }

}, [email, password]) 
    
return ( 

    (user.email !== null) ?
    <Navigate to="/courses"/>
    :
    <Form onSubmit={(e) => loginUser(e)}>
    <h2>Login</h2>
    <Form.Group controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control 
                type="email" 
                placeholder="Enter email" 
                value={email}
                onChange={e => setEmail(e.target.value)}
                required
            />
        </Form.Group>
        <Form.Group controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control 
                type="password" 
                placeholder="Password"
                value={password}
                onChange={e => setPassword(e.target.value)} 
                required
            />
        </Form.Group>


    { isActive ?
      <Button variant="success" type="submit" id="submitBtn">
        Login
      </Button>
      :
      <Button variant="success" type="submit" id="submitBtn" disabled>
        Login
      </Button>
    }
    </Form>
  );
}
