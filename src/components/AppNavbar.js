import {Link, NavLink} from 'react-router-dom'
import { useContext } from 'react';
import { Container } from 'react-bootstrap';

import UserContext from '../UserContext';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

export default function AppNavbar() {

  const { user } = useContext(UserContext)

  return (
    <Container>
      <Navbar  expand="lg">

          <Navbar.Brand as={Link} to="/">Zuitt</Navbar.Brand>
          
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">

              <Nav.Link as={NavLink} to="/">Home</Nav.Link>
              <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>

              {(user.email !== null) ?
                <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                :
                <>
                <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                </>
              }
            </Nav>
          </Navbar.Collapse>
      </Navbar>
    </Container>  
  );
}